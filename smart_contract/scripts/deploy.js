const main = async () => {
  const jknFactory = await hre.ethers.getContractFactory("JKNChain");
  const jknContract = await jknFactory.deploy();

  await jknContract.deployed();

  console.log("JKNChain address: ", jknContract.address);
};

const runMain = async () => {
  try {
    await main();
    process.exit(0);
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

runMain();