// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.4.22 <0.9.0;

contract JKNChain {
    struct User {
        address id;
        string name;
        string email;
        string password;
        string location;
        string nik;
        string cid;
    }

    struct Admin {
        address id;
        string name;
        string password;
    }

    mapping (address => User) public users;

    mapping (address => Admin) public admin;

    event UserAdded(address id, string name, string email, string password, string location, string nik, string cid);
    event AdminAdded(address id, string name, string password);

    function register(
        address _id, string memory _name, string memory _email, string memory _password, string memory _location, string memory _nik, string memory _cid
    ) public {
        require(users[_id].id != _id, "This user already exists.");
        users[_id].id = _id;
        users[_id].name = _name;
        users[_id].email = _email;
        users[_id].password = _password;
        users[_id].location = _location;
        users[_id].nik = _nik;
        users[_id].cid = _cid;
        emit UserAdded(_id, _name, _email, _password, _location, _nik, _cid);
    }

    function login(address _id, string memory _email, string memory _password) public view returns (bool) {
        return users[_id].id == _id && compareStrings(users[_id].email, _email) && compareStrings(users[_id].password, _password) ;
    }

    function compareStrings(string memory a, string memory b) public pure returns (bool) {
        return keccak256(abi.encodePacked(a)) == keccak256(abi.encodePacked(b));
    }

    function addCID(address _id, string memory _cid) public {
        users[_id].cid = _cid;
    }


    function addAdmin(address _id, string memory _name, string memory _password) public {
        require(admin[_id].id != _id, "This user already exists.");
        admin[_id].id = _id;
        admin[_id].name = _name;
        admin[_id].password = _password;
        emit AdminAdded(_id, _name, _password);
    }

    function isAdmin(address _id, string memory _password) public view returns (bool) {
        return admin[_id].id == _id && compareStrings(admin[_id].password, _password);
    }
}