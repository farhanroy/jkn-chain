import React, { useState, useContext } from "react";
import { useNavigate } from "react-router-dom";

import { EthereumContext } from "../context/EthereumContext";
import ipfs from "../utils/ipfs";

const HomePage = () => {
  const [file, setFile] = useState();
  const [address, setAddress] = useState("");
  const { addCID, isLoading } = useContext(EthereumContext);

  const handleChangeAddress = (e) => {
    setAddress({ ...address, [e.target.name]: e.target.value.replaceAll('"', "'").toLowerCase() });
  };

  function handleChange(e) {
    const files = e.target.files[0];
    setFile(files);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    // const res = await ipfs.add(file);
    // console.log(res.path);

    addCID(address, "");
  };

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col col-6">
          <div className="card" style={{ marginTop: "180px" }}>
            <div className="card-header">Kartu BPJS</div>
            <div className="card-body">
              <form>
                <div className="mb-3">
                  <label htmlFor="exampleInputEmail1" className="form-label">
                    Wallet address
                  </label>
                  <input
                    type="text"
                    name="address"
                    onChange={handleChangeAddress}
                    className="form-control"
                    id="exampleInputEmail1"
                    aria-describedby="emailHelp"
                  />
                  <div id="emailHelp" className="form-text">
                    Masukan address dari user JKN
                  </div>
                </div>
                <div className="mb-3">
                  <label className="form-label">Gambar</label>
                  <input
                    type="file"
                    className="form-control"
                    onChange={handleChange}
                  />
                </div>
                <button
                  type="submit"
                  className="btn btn-primary"
                  onClick={handleSubmit}
                >
                  Kirimkan
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomePage;
