import React, { useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import { EthereumContext } from "../context/EthereumContext";

const RegisterPage = () => {
    const navigate = useNavigate();
    const { isLoading, addAdmin } = useContext(EthereumContext);
    const [auth, setAuth] = useState({ name: "", password: "" });

    const handleChange = (e) => {
        setAuth({ ...auth, [e.target.name]: e.target.value });
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        await addAdmin(auth.name, auth.password);

        if (!isLoading) {
            navigate("/home");
        }

    }
  return (
    <div className="text-center">
      <main className="form-signin w-50 m-auto">
        <form style={{ marginTop: "150px" }}>
          <img
            className="mb-4"
            src="/logo_jkn.png"
            alt=""
            width={57}
            height={57}
          />
          <h1 className="h3 mb-3 fw-normal">Masukan akun admin</h1>
          <div className="form-floating mb-3 mt-3">
            <input
              type="text"
              name="name"
              className="form-control"
              id="floatingInput"
              placeholder="Name"
            />
            <label htmlFor="floatingInput">Nama</label>
          </div>
          <div className="form-floating mb-5">
            <input
              type="password"
              name="password"
              className="form-control"
              id="floatingPassword"
              placeholder="Password"
              onChange={handleChange}
            />
            <label htmlFor="floatingPassword">Password</label>
          </div>
          <button
            className="w-100 btn btn-lg btn-primary"
            type="submit"
            onClick={handleSubmit}
          >
            Sign in
          </button>
        </form>
      </main>
    </div>
  );
};

export default RegisterPage;
