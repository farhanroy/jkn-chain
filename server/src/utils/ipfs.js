import { create } from "ipfs-http-client";

const projectId = '2EhBIaL4XdZz5TUvKRmVtJKic2x'
const projectSecret = '2d3da3504c0725979108711ed401a547'
const auth = 'Basic ' + btoa(projectId + ':' + projectSecret)

const ipfs = create({
  host: 'ipfs.infura.io',
  port: 5001,
  protocol: 'https',
  headers: {
    authorization: auth,
  },
})

export default ipfs
