import React, { useEffect, useState } from "react";
import { ethers } from "ethers";

import { contractABI, contractAddress } from "../utils/constants";

export const EthereumContext = React.createContext();

const { ethereum } = window;

const createEthereumContract = () => {
  const provider = new ethers.providers.Web3Provider(ethereum);
  const signer = provider.getSigner();
  const contract = new ethers.Contract(contractAddress, contractABI, signer);

  return contract;
};

export const EthereumProvider = ({ children }) => {
  const [currentAccount, setCurrentAccount] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [userRole, setUserRole] = useState(false);

  const getUserRole = async (password) => {
    try {
      if (ethereum) {      
        const contract = createEthereumContract();
        const isAdmin = await contract.isAdmin(currentAccount, password);
        console.log(password);
        return isAdmin;

      } else {
        console.log("No ethereum object");
      }
    } catch (error) {
      console.log(error);
    }
  };

  const addAdmin = async (name, password) => {
    try {
      if (ethereum) {      
        const contract = createEthereumContract();
        const hash = await contract.addAdmin(currentAccount, name, password);

        setIsLoading(true);
        await hash.wait();
        setIsLoading(false);

      } else {
        console.log("No ethereum object");
      }
    } catch (error) {
      console.log(error);
    }
  };

  const addCID = async (address, cid) => {
    try {
      if (ethereum) {      
        console.log(address);
        const contract = createEthereumContract();
        const replaced = ethers.utils.getAddress(address);
        
        const hash = await contract.addCID(address, cid);

        setIsLoading(true);
        await hash.wait();
        setIsLoading(false);
        window.location.reload();

      } else {
        console.log("No ethereum object");
      }
    } catch (error) {
      console.log(error);
    }
  }

  const connectWallet = async () => {
    try {
      if (!ethereum) return alert("Please install MetaMask.");

      const accounts = await ethereum.request({
        method: "eth_requestAccounts",
      });

      setCurrentAccount(accounts[0]);
      window.location.reload();
    } catch (error) {
      console.log(error);

      throw new Error("No ethereum object");
    }
  };

  const checkIfWalletIsConnect = async () => {
    try {
      if (!ethereum) return alert("Please install metamask!");

      const accounts = await ethereum.request({ method: "eth_accounts" });

      if (accounts.length) {
        setCurrentAccount(accounts[0]);

        //getAllTransactions();
      } else {
        console.log("No accounts found");
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    checkIfWalletIsConnect();
  }, []);

  return (
    <EthereumContext.Provider
      value={{
        currentAccount,
        getUserRole,
        addAdmin,
        isLoading,
        addCID
      }}
    >
      {children}
    </EthereumContext.Provider>
  );
};
