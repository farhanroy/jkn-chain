import React from 'react'
import ReactDOM from 'react-dom/client'

import App from './App'
import { EthereumProvider } from './context/EthereumContext'

ReactDOM.createRoot(document.getElementById('root')).render(
  <EthereumProvider>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </EthereumProvider>
)
