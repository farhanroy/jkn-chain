import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:web3dart/web3dart.dart';

import '../models/user_model.dart';
import '../utils/constants.dart';

class UserService {

  late Client httpClient;
  late Web3Client ethereumClient;

  UserService() {
    httpClient = Client();
    ethereumClient = Web3Client(Constants.ethereumClientUrl, httpClient);
  }

  Future<void> register(UserModel userModel, String privateKey, String password) async {
    try {
      final contract = await getContract();
      final function = contract.function('register');

      Credentials key = EthPrivateKey.fromHex(Constants.privateKey);
      final addresss = await EthereumAddress.fromHex(userModel.id!);

      final result = await ethereumClient.sendTransaction(
          key,
          Transaction.callContract(
              contract: contract,
              function: contract.function("register"),
              parameters: [
                addresss,
                userModel.name,
                userModel.email,
                password,
                userModel.location,
                userModel.identityNumber,
                userModel.cid
              ]
          ),
        fetchChainIdFromNetworkId: true,
        chainId: null
      );
      print("Regitser: ${result}");
    } catch(e) {
      print(e);
      throw Exception(e);
    }
  }

  Future<bool> login(String address, String email, String password) async {
    final addresss = await EthereumAddress.fromHex(address);
    try {
      final contract = await getContract();
      final result = await ethereumClient.call(
          contract: contract,
          function: contract.function("login"),
          params: [
            addresss,
            email,
            password
          ]);
      print(result);
      return result[0];
    } catch(e) {
      throw Exception(e);
    }
  }

  Future<bool> isUserExist(EthereumAddress address) async {
    try {
      final contract = await getContract();
      final result = await ethereumClient.call(
          contract: contract,
          function: contract.function("getUserExists"),
          params: [address]);
      return result[0];
    } catch(e) {
      throw Exception(e);
    }
  }

  Future<DeployedContract> getContract() async {
    String abi = await rootBundle.loadString("assets/abi.json");
    String contractAddress = Constants.contractAddress;

    DeployedContract contract = DeployedContract(
      ContractAbi.fromJson(abi, Constants.contractName),
      EthereumAddress.fromHex(contractAddress),
    );

    return contract;
  }

  Future<void> getUserData(String address) async {
    final addresss = EthereumAddress.fromHex(address);
    try {
      final contract = await getContract();
      final result = await ethereumClient.call(
          contract: contract,
          function: contract.function("users"),
          params: [addresss]);
      print(result);
    } catch(e) {
      throw Exception(e);
    }
  }
}