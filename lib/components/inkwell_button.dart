import 'package:flutter/material.dart';

class InkwellButton extends StatelessWidget {
  const InkwellButton({Key? key, required this.image}) : super(key: key);

  final Image image;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SizedBox(
        width: 170,
        height: 60,
        child: image,
      ),
    );
  }
}
