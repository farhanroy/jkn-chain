class Constants {
  static const String contractName = 'JKNChain';
  static const String contractAddress = '0x3D3Eab73253B12472E37da72E18e3d8702646350';

  static const String privateKey = 'ccb3e439fef177d7ad5eb494e7ad605af3b5bcda0a2d4d2720e488a8e642e5bc';

  static const String ethereumClientUrl =
      'https://eth-goerli.g.alchemy.com/v2/DDVyMVBcZKmmwmViTdra6_V_MnKM5G58';
}