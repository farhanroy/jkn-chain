import 'package:get/get.dart';

import 'app_routes.dart';
import '../modules/splash/splash.dart';
import '../modules/login/login.dart';
import '../modules/register/register.dart';
import '../modules/home/home.dart';

class AppPages {
  static final List<GetPage> pages = [
    GetPage(
        name: AppRoutes.splash,
        binding: SplashBinding(),
        page: () => const SplashPage(),
    ),

    GetPage(
      name: AppRoutes.login,
      binding: LoginBinding(),
      page: () => const LoginPage(),
    ),

    GetPage(
      name: AppRoutes.register,
      binding: RegisterBinding(),
      page: () => const RegisterPage(),
    ),

    GetPage(
      name: AppRoutes.home,
      binding: HomeBinding(),
      page: () => const HomePage(),
    )
  ];
}