class AppRoutes {
  static const splash = '/splash';

  static const register = '/register';
  static const login = '/login';

  static const home = '/home';
}