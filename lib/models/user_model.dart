class UserModel {
  final String? id;
  final String? name;
  final String? email;
  final String? location;
  final String? identityNumber;
  final String? cid;

  UserModel(
      {this.id, this.name, this.email, this.location, this.identityNumber, this.cid});
}