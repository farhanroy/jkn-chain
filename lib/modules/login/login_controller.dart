import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:jkn_chain/routes/app_routes.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../services/user_service.dart';

class LoginController extends GetxController with StateMixin<String> {

  late UserService _userService;

  RxString address = RxString('');
  RxString email = RxString('');
  RxString password = RxString('');

  @override
  void onInit() {
    debugPrint('$runtimeType onInit called');
    super.onInit();
    _userService = UserService();
  }
  Future<void> submit() async {
    final preference = await SharedPreferences.getInstance();
    change('', status: RxStatus.loading());
    try {
      print("as: ${address.value}");
      final result = await _userService.login(address.value, email.value, password.value);
      if (result) {
        await preference.setString('address', address.value);
        Get.offNamed(AppRoutes.home);
        change('', status: RxStatus.success());
      } else {
        change('User tidak ada silahkan daftarkan akun', status: RxStatus.error());
        Get.defaultDialog(
            title: 'Gagal masuk',
            radius: 10.0,
            titlePadding: const EdgeInsets.only(top: 32.0),
            contentPadding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 32.0),
            content: Text('User tidak ada silahkan daftarkan akun')
        );
      }

    } catch(e) {
      change('', status: RxStatus.error());
      Get.defaultDialog(
          title: 'Gagal masuk',
          radius: 10.0,
          titlePadding: const EdgeInsets.only(top: 32.0),
          contentPadding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 32.0),
          content: Text('Maaf ada error')
      );
    }
  }

  void addressChanged(String val) => address.value = val;
  void emailChanged(String val) => email.value = val;
  void passwordChanged(String val) => password.value = val;

}