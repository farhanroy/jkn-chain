import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../routes/app_routes.dart';
import '../../components/common_button.dart';
import '../../components/common_text_field.dart';
import '../../components/inkwell_button.dart';
import '../../utils/app_colors.dart';
import 'login_controller.dart';

class LoginPage extends GetView<LoginController> {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 50),
            child: Center(
              child: Padding(
                padding:
                const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                        "Masuk",
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 26,
                        )),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(
                        "Tolong masukan akun anda untuk menggunakan fitur",
                        style: const TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 14,
                        )),
                    const SizedBox(
                      height: 150,
                    ),
                    CommonTextField(
                      Lone: "Address",
                      Htwo: "Address",
                      onChanged: (val) => controller.addressChanged(val),
                    ),
                    const SizedBox(height: 20),
                    CommonTextField(
                        Lone: "Email",
                        Htwo: "Email",
                      onChanged: (val) => controller.emailChanged(val),
                    ),
                    const SizedBox(height: 20),
                    CommonTextField(
                        Lone: "Password",
                        Htwo: "Password",
                      onChanged: (val) => controller.passwordChanged(val),
                    ),
                    const SizedBox(height: 20),

                    const SizedBox(height: 40),
                    InkWell(
                      child: SignUpContainer(st: "Masuk"),
                      onTap: () => controller.submit(),
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    InkWell(
                      child: RichText(
                        text: RichTextSpan(
                            one: "Belum punya akun ? ", two: "Daftar"),
                      ),
                      onTap: () => Get.toNamed(AppRoutes.register),
                    ),
                    //Text("data"),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  // rich text
  TextSpan RichTextSpan({required String one, required String two}) {
    return TextSpan(children: [
      TextSpan(
          text: one,
          style: TextStyle(fontSize: 13, color: AppColors.kBlackColor)),
      TextSpan(
          text: two,
          style: TextStyle(
            fontSize: 13,
            color: AppColors.kBlueColor,
          )),
    ]);
  }

  Widget SignUpContainer({required String st}) {
    return Container(
      width: double.infinity,
      height: 60,
      decoration: BoxDecoration(
        color: AppColors.kBlueColor,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Center(
        child: Text(
            st,
            style: const TextStyle(
              color: AppColors.kwhiteColor,
              fontWeight: FontWeight.normal,
              fontSize: 14,
            )),
      ),
    );
  }

}
