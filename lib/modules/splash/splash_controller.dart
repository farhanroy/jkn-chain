import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../routes/app_routes.dart';

class SplashController extends GetxController with StateMixin<String> {

  @override
  void onInit() {
    checkUserExist();
    super.onInit();
  }

  Future<void> checkUserExist() async {
    change('', status: RxStatus.loading());
    final preference = await SharedPreferences.getInstance();
    final data = preference.getString('address');

    print("User: $data");

    if (data != null) {
      change('', status: RxStatus.success());
      Get.offNamed(AppRoutes.home);
    } else {
      change('', status: RxStatus.empty());
    }
  }

}