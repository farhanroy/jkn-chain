import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../components/inkwell_button.dart';
import '../../routes/app_routes.dart';
import '../../utils/app_colors.dart';
import 'splash_controller.dart';

class SplashPage extends GetView<SplashController> {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 30),
            child: Center(
              child: Padding(
                padding:
                const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                        "JKN Chain",
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 26,
                        )),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(
                        "Makin secure dengan aplikasi JKN Chain",
                        style: const TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 14,
                        )),
                    const SizedBox(
                      height: 120,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('image/img2.png')
                        )
                      ),
                    ),
                    const SizedBox(
                      height: 200,
                    ),
                    InkWell(
                      child: SignUpContainer(st: "Daftar"),
                      onTap: () {
                        Get.toNamed(AppRoutes.register);
                      },
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    InkWell(
                      child: RichText(
                        text: RichTextSpan(
                            one: "Belum punya akun ? ", two: "Masuk"),
                      ),
                      onTap: () {
                        Get.toNamed(AppRoutes.login);
                      },
                    ),
                    //Text("data"),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  TextSpan RichTextSpan({required String one, required String two}) {
    return TextSpan(children: [
      TextSpan(
          text: one,
          style: TextStyle(fontSize: 13, color: AppColors.kBlackColor)),
      TextSpan(
          text: two,
          style: TextStyle(
            fontSize: 13,
            color: AppColors.kBlueColor,
          )),
    ]);
  }

  Widget SignUpContainer({required String st}) {
    return Container(
      width: double.infinity,
      height: 60,
      decoration: BoxDecoration(
        color: AppColors.kBlueColor,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Center(
        child: Text(
            st,
            style: const TextStyle(
              color: AppColors.kwhiteColor,
              fontWeight: FontWeight.normal,
              fontSize: 14,
            )),
      ),
    );
  }

}
