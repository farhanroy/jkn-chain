import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jkn_chain/components/common_button.dart';
import 'package:jkn_chain/routes/app_routes.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'home_controller.dart';

class HomePage extends GetView<HomeController> {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    controller.checkUser();
    return Scaffold(
      body: Center(
        child: CommonButton(
            title: 'Logout',
            onPressed: () {
              clearSession().then((value) {
                Get.offNamed(AppRoutes.splash);
              });
            }),
      ),
    );
  }

  Future<void> clearSession() async {
    final pref = await SharedPreferences.getInstance();
    await pref.clear();
  }

}
