import 'package:get/get.dart';
import 'package:jkn_chain/services/user_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeController extends GetxController {

  RxString text = RxString('');

  final _userService = UserService();

  @override
  void onInit() {
    super.onInit();
    checkUser();
  }

  Future<void> checkUser() async {
    final preference = await SharedPreferences.getInstance();
    String address = preference.getString('address') ?? "";

    _userService.getUserData(address);
  }

}