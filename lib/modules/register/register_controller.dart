import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../models/user_model.dart';
import '../../routes/app_routes.dart';
import '../../services/user_service.dart';

class RegisterController extends GetxController with StateMixin<String> {

  RxString address = RxString('');
  RxString privateKey = RxString('');
  RxString name = RxString('');
  RxString email = RxString('');
  RxString password = RxString('');
  RxString location = RxString('');
  RxString identityNumber = RxString('');

  late UserService _userService;

  @override
  void onInit() {
    debugPrint('$runtimeType onInit called');
    super.onInit();
    _userService = UserService();
  }

  @override
  void onClose() {
    super.onClose();
    debugPrint('$runtimeType onClose called');
  }

  Future<void> submit() async {
    change('', status: RxStatus.loading());
    try {
      final preference = await SharedPreferences.getInstance();
      final userModel = UserModel(
        id: address.value,//userAddress,
        name: name.value,
        email: email.value,
        location: location.value,
        identityNumber: identityNumber.value,
        cid: ""
      );
      await _userService.register(userModel, privateKey.value, password.value);
      await preference.setString('address', address.value);
      change('', status: RxStatus.success());
      Get.offNamed(AppRoutes.home);
      print('success');
    } catch(e) {
      change('', status: RxStatus.error(e.toString()));
    }
  }

  void addressChanged(String val) => address.value = val;
  void privateKeyChanged(String val) => privateKey.value = val;
  void nameChanged(String val) => name.value = val;
  void emailChanged(String val) => email.value = val;
  void passwordChanged(String val) => password.value = val;
  void locationChanged(String val) => location.value = val;
  void identityNumberChanged(String val) => identityNumber.value = val;

}