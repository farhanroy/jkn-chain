import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../components/common_button.dart';
import '../../components/common_text_field.dart';
import '../../components/inkwell_button.dart';
import '../../routes/app_routes.dart';
import '../../utils/app_colors.dart';
import 'register_controller.dart';

class RegisterPage extends GetView<RegisterController> {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 13),
            child: Center(
              child: Padding(
                padding:
                const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                        "Sign Up",
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 26,
                        )),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(
                        "Please sign up to enter in a app.",
                        style: const TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 14,
                        )),
                    const SizedBox(
                      height: 40,
                    ),

                    const SizedBox(height: 20),
                    CommonTextField(
                        Lone: "Address", 
                        Htwo: "Meta Address",
                      onChanged: (val) => controller.addressChanged(val),
                    ),
                    const SizedBox(height: 20),
                    CommonTextField(
                        Lone: "Private Key",
                        Htwo: "Private Key",
                        onChanged: (val) => controller.privateKeyChanged(val)
                    ),
                    const SizedBox(height: 20),
                    CommonTextField(
                        Lone: "Email",
                        Htwo: "Email",
                        onChanged: (val) => controller.emailChanged(val)
                    ),
                    const SizedBox(height: 20),
                    CommonTextField(
                        Lone: "Name",
                        Htwo: "Name",
                        onChanged: (val) => controller.nameChanged(val)
                    ),
                    const SizedBox(height: 20),
                    CommonTextField(Lone: "Password", Htwo: "Password", isObscure: true,
                        onChanged: (val) => controller.passwordChanged(val)),
                    const SizedBox(height: 20),
                    CommonTextField(
                        Lone: "No. KTP",
                        Htwo: "NIK",
                        onChanged: (val) => controller.identityNumberChanged(val)
                    ),
                    const SizedBox(height: 20),
                    CommonTextField(
                        Lone: "Alamat",
                        Htwo: "Alamat",
                        onChanged: (val) => controller.locationChanged(val)
                    ),
                    const SizedBox(height: 20),

                    const SizedBox(height: 40),
                    InkWell(
                      child: SignUpContainer(st: "Sign Up"),
                      onTap: () => controller.submit(),
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    InkWell(
                      child: RichText(
                        text: RichTextSpan(
                            one: "Already have an account ? ", two: "Login"),
                      ),
                      onTap: () {

                      },
                    ),
                    //Text("data"),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  // rich text
  TextSpan RichTextSpan({required String one, required String two}) {
    return TextSpan(children: [
      TextSpan(
          text: one,
          style: TextStyle(fontSize: 13, color: AppColors.kBlackColor)),
      TextSpan(
          text: two,
          style: TextStyle(
            fontSize: 13,
            color: AppColors.kBlueColor,
          )),
    ]);
  }

  Widget SignUpContainer({required String st}) {
    return Container(
      width: double.infinity,
      height: 60,
      decoration: BoxDecoration(
        color: AppColors.kBlueColor,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Center(
        child: Text(
            st,
            style: const TextStyle(
              color: AppColors.kwhiteColor,
              fontWeight: FontWeight.normal,
              fontSize: 14,
            )),
      ),
    );
  }
}


